FROM node:latest

ADD . /opt/
WORKDIR /opt/
ARG app_env
ENV LANG en_US.UTF-8
ENV TZ=Europe/Kiev

RUN npm install && npm run build:prod

CMD [ "npm", "run", "serve"]
