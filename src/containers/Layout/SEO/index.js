import React from "react"
import PropTypes from "prop-types"
import Helmet from "react-helmet"

function SEO({ description, meta, title, link }) {
  const metaDescription = description

  return (
    <Helmet
      htmlAttributes={{
        lang: "uk",
      }}
      title={title}
      meta={[
        {
          name: `description`,
          content: metaDescription,
        },
        {
          name: `description`,
          content: metaDescription,
        },
        {
          property: `og:title`,
          content: title,
        },
        {
          property: `og:url`,
          content: link,
        },
        {
          property: `og:image`,
          content: "https://stirtrek.com/images/header-logo.png",
        },
        {
          property: `og:image:secure_url`,
          content: "https://stirtrek.com/images/header-logo.png",
        },
        {
          property: `og:image:type`,
          content: "image/png",
        },
        {
          property: `og:description`,
          content: metaDescription,
        },
        {
          property: `og:type`,
          content: `website`,
        },
        {
          name: `twitter:card`,
          content: `summary`,
        },
        {
          name: `twitter:creator`,
          content: "TAS BANK",
        },
        {
          name: `twitter:title`,
          content: title,
        },
        {
          name: `twitter:description`,
          content: metaDescription,
        },
        { name: "author", content: "TypeError" },
      ].concat(meta)}
    />
  )
}

SEO.defaultProps = {
  lang: `uk`,
  meta: [],
  description: ``,
}

SEO.propTypes = {
  description: PropTypes.string,
  lang: PropTypes.string,
  meta: PropTypes.arrayOf(PropTypes.object),
  title: PropTypes.string.isRequired,
}

export default SEO
