//Core
import React, { useEffect, useState } from "react"

//Components
import Main from "../components/Main"
import BrownZone from "../components/BrownZone"
import Loader from "../components/Loader"
import SEO from "../containers/Layout/SEO"

import "../styles/main.scss"

const IndexPage = () => {
  const [show, setshow] = useState(true)

  const hideLoader = () => setshow(false)

  useEffect(() => {
    setTimeout(() => {
      hideLoader()
    }, 2000)
    console.log("loaded")
  }, [])

  return (
    <>
      <SEO
        title={`Tech & a Flick - Stir Trek`}
        description={`Stir Trek is a one-day conference in Columbus, Ohio focused on teaching software developers the latest and greatest in technologies, techniques, and tools. The full day of content is always concluded with a private screening of a blockbuster film on its opening day.`}
      ></SEO>
      <Loader loaded={show}></Loader>

      <main style={{ display: "grid" }}>
        <Main></Main>
        <div style={{ position: "relative" }}>
          <BrownZone></BrownZone>
        </div>
      </main>
    </>
  )
}

export default IndexPage
