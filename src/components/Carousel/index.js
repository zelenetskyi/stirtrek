//Core
import React, { useState, useEffect } from "react"

//Styles
import style from "./style.module.scss"

//Images
import next_yellow from "../../images/carousel_yellow_next.svg"

const {
  Container,
  AbsoluteWrapper,
  CarouselWrapper,
  FirstImage,
  SecondImage,
  ThirdImage,
  QuickGoButtonsWrapper,
  QuickGoButton,
  Title,
  Description,
  DescriptionTitle,
  Additional,
  NextYellow,
  Current,
  ButtonsContainer,
  Content,
} = style

let images = [
  "https://images.unsplash.com/photo-1470813740244-df37b8c1edcb?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1351&q=80",
  "https://images.unsplash.com/photo-1436968188282-5dc61aae3d81?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1353&q=80 ",
  `https://images.unsplash.com/photo-1413919873593-061d76ec8452?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1351&q=80`,
  `https://images.unsplash.com/photo-1508150630776-68b08eef865e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80`,
  "https://images.unsplash.com/photo-1470071459604-3b5ec3a7fe05?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1440&q=80",
  `https://images.unsplash.com/photo-1580137189272-c9379f8864fd?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80`,
]

const texts = [
  `  48 sessions of
 critical content`,
  `  58 sessions of
 critical content`,
  `  18 sessions of
 critical content`,
  `  28 sessions of
 critical content`,
  `  38 sessions of
 critical content`,
  ` 68 sessions of
 critical content`,
]

const EVENTS = [
  {
    title: "Madison Red",
    subTitle: "Campaign",
    main: "Drink Tea With Benedict Cumberbatch",
    link: "https://www.google.com/",
  },
  {
    title: "Madison Red",
    subTitle: "Campaign ",
    main: "Drink Tea With Benedict Cumberbatch",
    link: "https://www.google.com/",
  },
  {
    title: "Madison Red",
    subTitle: "Campaign ",
    main: "Drink Tea With Benedict Cumberbatch",
    link: "https://www.google.com/",
  },
  {
    title: "Madison Red",
    subTitle: "Campaign ",
    main: "Drink Tea With Benedict Cumberbatch",
    link: "https://www.google.com/",
  },
  {
    title: "Madison Red",
    subTitle: "Campaign ",
    main: "Drink Tea With Benedict Cumberbatch",
    link: "https://www.google.com/",
  },
  {
    title: "Madison Black",
    subTitle: "Campaign ",
    main: "Drink Tea With Benedict Cumberbatch",
    link: "https://www.google.com/",
  },
]

const classes = [FirstImage, SecondImage, ThirdImage]
const TIME = 1000 * 10

const Carousel = props => {
  const [currentImage, setCurrentImage] = useState(0)
  const [event, setEvent] = useState(0)

  useEffect(() => {
    const timer = setInterval(() => {
      setCurrentImage(currentImage + 1 >= images.length ? 0 : currentImage + 1)
    }, TIME)

    setEvent(EVENTS[currentImage])
    return () => {
      clearInterval(timer)
    }
  }, [currentImage])

  const chooseImage = index => setCurrentImage(index)

  return (
    <section className={Container}>
      <div className={AbsoluteWrapper}>
        <div className={CarouselWrapper}>
          {images.slice(currentImage).map((image, index) => (
            <a href={EVENTS[index].link}>
              <div key={index} className={classes[index]}>
                <img src={image} alt={"tests"}></img>
              </div>
            </a>
          ))}
        </div>
        <div className={Content}>
          <a href={EVENTS[currentImage].link}>
            <div className={Title}>{texts[currentImage]}</div>
            <img
              src={next_yellow}
              className={NextYellow}
              alt="next_yellow | STIR TREK"
            ></img>
          </a>
        </div>
        <div className={Description}>
          <div className={DescriptionTitle}>{event.title}</div>
          <div className={Additional}>
            {event.subTitle}
            <a href={event.link} style={{ textDecoration: "none" }}>
              <ins>{" " + event.main}</ins>
            </a>
          </div>
        </div>
        <div className={ButtonsContainer}>
          <div className={QuickGoButtonsWrapper}>
            {Array.from(new Array(images.length)).map((_, index) => (
              <div
                onClick={() => chooseImage(index)}
                key={index}
                className={[
                  QuickGoButton,
                  currentImage === index ? Current : " ",
                ].join(" ")}
              />
            ))}
          </div>
        </div>
      </div>
    </section>
  )
}

export default Carousel
