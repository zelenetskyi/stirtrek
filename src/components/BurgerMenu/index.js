//Core
import React, { useState } from "react"

//Styles
import style from "./style.module.scss"
import jump from "jump.js"

//Images
import logo_mobile from "../../images/logo_mobile.svg"

const { Container, Links, Burger, Menu, open, List, close } = style

const BurgerMenu = () => {
  const [showMenu, setshowMenu] = useState(false)

  const changeVision = () => setshowMenu(!showMenu)

  return (
    <div className={Burger}>
      <div className={Container}>
        <img src={logo_mobile} alt="logo | STIR TREK"></img>
        <div className={Links}>
          <div onClick={() => jump("#courses")}>Schedule</div>
          <div onClick={() => jump("#courses")}>Speakers</div>
        </div>
        <Dots click={changeVision}></Dots>
      </div>
      <div
        onClick={changeVision}
        className={[Menu, showMenu ? open : " "].join(" ")}
      >
        <div>
          <div onClick={changeVision} className={close}></div>

          <ul className={List}>
            <li>
              <a href="https://stirtrek.com/Info/FAQ/">
                Frequently Asked Questions
              </a>
            </li>
            <li>
              <a href="https://stirtrek.com/Info/Travel/">Travel Information</a>
            </li>
            <li>
              <a href="https://stirtrek.com/Info/Contact/">Contact Us</a>
            </li>
            <li>
              <a href="https://stirtrek.com/Info/CodeOfConduct/">
                Code Of Conduct
              </a>
            </li>
            <li>
              <a href="https://stirtrek.com/Info/CommitmentToDiversity/">
                Commitment to Diversity
              </a>
            </li>
            <li>
              <a href="https://stirtrek.com/Info/CodeOfConduct/">
                Anti-Harassment Policy
              </a>
            </li>
            <li>
              <a href="https://stirtrek.com/Info/CodeOfConduct/">
                Privacy Policy
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  )
}

export default BurgerMenu

const Dots = ({ click }) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="4"
    height="16"
    viewBox="0 0 4 16"
    style={{ cursor: "pointer" }}
    onClick={click}
  >
    <g id="Group_9194" data-name="Group 9194" transform="translate(-380 -94)">
      <g id="Group_9053" data-name="Group 9053" transform="translate(380 94)">
        <circle
          id="Ellipse_186"
          data-name="Ellipse 186"
          cx="2"
          cy="2"
          r="2"
          fill="#fff"
        />
        <circle
          id="Ellipse_187"
          data-name="Ellipse 187"
          cx="2"
          cy="2"
          r="2"
          transform="translate(0 6)"
          fill="#fff"
        />
        <circle
          id="Ellipse_188"
          data-name="Ellipse 188"
          cx="2"
          cy="2"
          r="2"
          transform="translate(0 12)"
          fill="#fff"
        />
      </g>
    </g>
  </svg>
)
