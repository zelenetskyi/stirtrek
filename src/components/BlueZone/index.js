//Core
import React, { useRef, useState } from "react"

//Styles
import style from "./style.module.scss"
import "./menu.scss"

//Images
import getStarted from "../../images/get_started_button.svg"
import man from "../../images/man.svg"
import logo from "../../images/logo_header.svg"
import logo_mobile from "../../images/logo_mobile.svg"

//Helpreps
import { cumulativeOffset } from "../../helpers/calc"

//Utils
import jump from "jump.js"

const {
  MainText,
  Container,
  SecondText,
  LogoContainer,
  Logo,
  Man,
  TextWrapper,
  Start,
  text,
  LinkItem,
  DotsWrapper,
  img,
} = style

// $card.style.transform = `matrix3d(${matrix.toString()})`
const Header = props => {
  const card = useRef(null)
  const [showMenu, setShowMenu] = useState(false)

  const toggleMenu = () => setShowMenu(!showMenu)

  const buttonMove = e => {
    if (card) {
      const x = ((e.pageX - cumulativeOffset(card).left - 350 / 2) * -1) / 100
      const y = ((e.pageY - cumulativeOffset(card).top - 350 / 2) * -1) / 100

      const matrix = [
        [1, 0, 0, -x * 0.00015],
        [0, 1, 0, -y * 0.00015],
        [0, 0, 1, 1],
        [0, 0, 0, 1],
      ]

      card.current.style.transform = `matrix3d(${matrix.toString()})`
    }
  }

  return (
    <section className={Container} onMouseMove={buttonMove}>
      <div className={LogoContainer}>
        <div>
          <a href="/">
            <img
              draggable="false"
              src={logo}
              alt="logo header | STIR TREK"
              className={Logo}
            ></img>
          </a>
        </div>
        <Dots click={toggleMenu} show={showMenu}></Dots>

        <Link link="/" text="Schedule"></Link>
        <Link link="/" text="Speakers"></Link>
      </div>

      <div className={TextWrapper}>
        <h1 className={MainText}>Meet The Community And Find Inspiration</h1>
        <h2 className={SecondText}>
          StirTrek is a one-day conference focused on teaching software
          developers, and others in the industry, the latest and greatest in
          technologies, techniques, and tools.
        </h2>
      </div>

      <div className={Start} ref={card}>
        <a href="/">
          <img
            src={getStarted}
            className={img}
            draggable="false"
            alt="Get Started | STIR TREK"
          ></img>
          <div className={text}>Get Started</div>
        </a>
      </div>

      <img
        src={man}
        draggable="false"
        className={Man}
        alt=" Ok-Man | STIR TREK"
      ></img>
    </section>
  )
}

export default Header

const Link = ({ link, text, to = false }) => (
  <div onClick={() => (to ? jump(to) : console.log(to))}>
    <a href={link} style={{ textDecoration: "none" }}>
      <span className={LinkItem}>{text}</span>
    </a>
  </div>
)

const Dots = ({ click, show }) => {
  return (
    <div className={DotsWrapper}>
      <div className={`open ${show ? "oppenned" : " "}`} onClick={click}>
        <span className="cls"></span>
        <span>
          <ul className="sub-menu ">
            <li>
              <a href="https://stirtrek.com/Info/FAQ/">
                Frequently Asked Questions
              </a>
            </li>
            <li>
              <a href="https://stirtrek.com/Info/Travel/">Travel Information</a>
            </li>
            <li>
              <a href="https://stirtrek.com/Info/Contact/">Contact Us</a>
            </li>
            <li>
              <a href="https://stirtrek.com/Info/CodeOfConduct/">
                Code Of Conduct
              </a>
            </li>
            <li>
              <a href="https://stirtrek.com/Info/CommitmentToDiversity/">
                Commitment to Diversity
              </a>
            </li>
            <li>
              <a href="https://stirtrek.com/Info/CodeOfConduct/">
                Anti-Harassment Policy
              </a>
            </li>
            <li>
              <a href="https://stirtrek.com/Info/CodeOfConduct/">
                Privacy Policy
              </a>
            </li>
          </ul>
        </span>
        <span className="cls"></span>
      </div>
    </div>
  )
}
