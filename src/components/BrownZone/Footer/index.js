//Core
import React from "react"

//Styles
import style from "./style.module.scss"

//Components
import facebook from "../../../images/facebook.svg"
import twitter from "../../../images/twitter.svg"
import header_logo from "../../../images/logo_header.svg"

const {
  Footer,
  title,
  social,
  SocialWrapper,
  Wrapper,
  InfoWrapper,
  annotation,
  Logo,
} = style

const FooterComponent = () => {
  return (
    <footer className={Footer}>
      <div className={Wrapper}>
        <div className={InfoWrapper}>
          <div>
            <div className={title}>LET'S GET CONNECTED.</div>
            <a href={"mailto:info@stirtrek.com"}>
              <span>info@stirtrek.com</span>
            </a>
          </div>
          <div className={SocialWrapper}>
            <a href="/">
              <img src={facebook} className={social} alt={social}></img>
            </a>
            <a href="/">
              <img src={twitter} className={social} alt={social}></img>
            </a>
          </div>
        </div>

        <div className={annotation}>
          All Rights Reserved. Copyright 2019 Stir Trek Conference.
        </div>
      </div>
      <a href="/">
        <img src={header_logo} className={Logo}></img>
      </a>
    </footer>
  )
}

export default FooterComponent
