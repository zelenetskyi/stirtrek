//Core
import React from "react"

//Styles
import style from "./style.module.scss"

//Componennts
import Courses from "./Courses"
import Sponsors from "./Sponsors"
import Footer from "./Footer"

const { Container, Wrapper, Triangle, first, second } = style

const BrownZone = props => {
  return (
    <section className={Container}>
      <div className={Wrapper}>
        <Courses></Courses>
        <Sponsors></Sponsors>
        <Footer></Footer>
        <div className={[Triangle, first].join(" ")}></div>
        <div className={[Triangle, second].join(" ")}></div>
      </div>
    </section>
  )
}

export default BrownZone
