//Core
import React from "react"

//Styles
import style from "./style.module.scss"

//Images
import mentor from "../../../../images/mentor1.svg"

const {
  Container,
  Info,
  Main,
  View,
  Name,
  Time,
  MentorName,
  Image,
  Mentor,
  Type,
} = style

const Mentors = ({
  name = "How to avoid UX-traps with a deck of cards",
  date = "8:30 AM",
  image = mentor,
  mentorName = "Jessica Engströmdadasdadsa",
  type = "Design (UI, UX, IA, etc.)",
  mentorTitle = "Senior / Product Design",
  link = "/",
}) => {
  return (
    <div className={Container}>
      <a href={link}>
        <div className={Info}>
          <span className={Name}>{name}</span>
          <span className={Time}>{date}</span>
        </div>
      </a>

      <div className={Main}>
        <div className={Mentor}>
          <img
            src={image}
            className={Image}
            alt={mentorName + "| STIR TREK"}
          ></img>
          <span>{mentorName}</span>
        </div>
        <div className={Type}>{type}</div>
        <div>{mentorTitle}</div>
      </div>
      <a href="/">
        <div className={View}>View</div>
      </a>
    </div>
  )
}

export default Mentors
