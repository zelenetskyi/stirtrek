//Core
import React, { useState } from "react"

//Styles
import style from "./style.module.scss"

//Compoents
import CoursCard from "./CoursCard"
import MentorItem from "./MentorItem"

//Images
import first_cours_card from "../../../images/first_cours_card.svg"
import second_course_card from "../../../images/second_course_card.svg"

const {
  Courses,
  TextWrapper,
  CoursWrapper,
  CoursWrapperMobile,
  MentorsWrapper,
  QuickGoButtonsWrapper,
  QuickGoButton,
  Current,
} = style

const Images = [
  { image: first_cours_card },
  { image: second_course_card },
  { x: 1 },
  { x: 1 },
]

const CoursesComponent = () => {
  const [slide, setSlide] = useState(0)

  const changeActiveImage = index => setSlide(index)

  return (
    <section className={Courses} id="courses">
      <h3 className={TextWrapper}>INTENSIVE COURSES</h3>
      <div className={CoursWrapper}>
        <CoursCard></CoursCard>
        <CoursCard image={second_course_card}></CoursCard>
        <CoursCard></CoursCard>
        <CoursCard></CoursCard>
      </div>
      <div className={CoursWrapperMobile}>
        <div style={{ width: "100%" }}>
          <CoursCard {...Images[slide]} />
        </div>
        <div className={QuickGoButtonsWrapper}>
          {Array(Images.length)
            .fill(1)
            .map((_, index) => (
              <div
                key={index}
                onClick={() => changeActiveImage(index)}
                className={[
                  QuickGoButton,
                  index === slide ? Current : " ",
                ].join(" ")}
              ></div>
            ))}
        </div>
      </div>
      <div className={MentorsWrapper}>
        <MentorItem></MentorItem>
        <MentorItem></MentorItem>
        <MentorItem></MentorItem>
        <MentorItem></MentorItem>
        <MentorItem></MentorItem>
      </div>
    </section>
  )
}

export default CoursesComponent
