//Core
import React from "react"

//Styles
import style from "./style.module.scss"

import first_cours_card from "../../../../images/first_cours_card.svg"
import "./card.css"

const {
  InfoWrapper,
  Time,
  Name,
  Price,
  Image,
  BackSide,
  Container,
  Card,
  Test,
  Box,
  DateWrapper,
  BoxWrapper,
} = style

const CoursCard = ({
  time = 10,
  coursName = "DevOps / ALM",
  price = 15,
  date = "05.12.19",
  image = first_cours_card,
  link,
}) => {
  return (
    <div className={Card}>
      <div style={{ width: "100%" }}>
        <div className={Container}>
          <img
            src={image}
            className={Image}
            alt={coursName + "| STIR TREK"}
          ></img>
          <div className={InfoWrapper}>
            <div>
              <div className={Time}>{`${time} Days Left`}</div>
              <div className={Name}>{coursName}</div>
              <div className={Price}>From ${price}</div>
            </div>
          </div>
        </div>
        <div className={BackSide}>
          <div className={DateWrapper}>{date}</div>
          <div className={Test}>Testing Quality</div>
          <a href="/">
            <div className="wrapper">
              <div className="card">Learn More</div>
            </div>
          </a>
        </div>
      </div>
    </div>
  )
}

export default CoursCard
