//Core
import React from "react"

//Styles
import style from "./style.module.scss"

//Components
import Sponsor from "./Sponsor"

//Images
import sponsors from "../../../images/sponsors.svg"
import boldpenguin from "../../../images/boldpenguin_350x200.png"
import IGS_sponsor from "../../../images/IGS_sponsor.svg"
import workstate from "../../../images/workstate_350x200.svg"
import insight from "../../../images/insight_350x200.svg"

const {
  Container,
  Year,
  Annotation,
  Introduction,
  TitleWrapper,
  Sponsors,
  Image,
} = style

const BrownZone = props => {
  return (
    <div className={Container}>
      <div className={Introduction}>
        <div className={TitleWrapper}>
          <div className={Year}>2019</div>
          <h4>Sponsors</h4>
        </div>
        <div className={Annotation}>
          Interested in sponsorship opportunities? Email us:
          <a href="mailto:info@stirtrek.com">
            <span> info@stirtrek.com</span>
          </a>
        </div>
        <div className={Sponsors}>
          <Sponsor></Sponsor>
          <Sponsor logo={IGS_sponsor} name={"IGS"} rotate={true}></Sponsor>
          <Sponsor name={"WORKSTATE"} logo={workstate}></Sponsor>
          <Sponsor logo={insight} rotate={true} name={"INSIGHT"}></Sponsor>
          <Sponsor logo={boldpenguin} name="bold penguin">
            {" "}
          </Sponsor>
          <Sponsor rotate={true} hide={true}></Sponsor>
        </div>
        <img src={sponsors} className={Image} alt={"Sponsors"}></img>
      </div>
    </div>
  )
}

export default BrownZone
