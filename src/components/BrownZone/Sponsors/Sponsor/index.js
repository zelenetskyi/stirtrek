//Core
import React, { useState } from "react"

//Styles
import style from "./style.module.scss"

//Images
import triangle_sponsors from "../../../../images/triangle_sponsors.svg"
import logo_awn from "../../../../images/awh.svg"
// import logo_awn from "../../../../images/IGS_sponsor.svg"
import next from "../../../../images/next.svg"

const {
  Sponsor,
  Triangle,
  Top,
  Logo,
  Name,
  Rotate,
  Next,
  hidden,
  up,
  move,
} = style

const SponsorComponents = ({
  name = "AwH",
  logo = logo_awn,
  rotate = false,
  hide = false,
}) => {
  const [animate, setAnimate] = useState(false)

  const changeAnimation = () => setAnimate(!animate)

  return (
    <section
      className={[Sponsor, rotate ? Top : " ", hide ? hidden : " "].join(" ")}
      onMouseEnter={changeAnimation}
      onMouseLeave={changeAnimation}
    >
      <div className={[Triangle, animate ? up : " "].join(" ")}>
        <img
          src={triangle_sponsors}
          className={rotate ? Rotate : ""}
          alt="triangle_sponsors"
        ></img>
        <img src={logo} className={Logo} alt={name + " sponsor"}></img>
      </div>
      <div className={Name}>
        <span>{name}</span>
        <img
          className={[Next, animate ? move : ""].join(" ")}
          src={next}
          alt="next icon"
        ></img>
      </div>
    </section>
  )
}

export default SponsorComponents
