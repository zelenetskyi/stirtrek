//Core
import React from "react"

//Styles
import style from "./style.module.scss"

//Components
import BlueZone from "../BlueZone"
import YellowZone from "../YellowZone"
import RedZone from "../RedZone"
import BlackZone from "../BlackZone"
import Loader from "../Loader"
import BurgerMenu from "../BurgerMenu"

const { Container } = style

//Доробить каресель
// Попросить адекватну картинку для
/***
 * спонсорів
 * чорну вдову
 *
 *
 * для карток
 * запистати за текс на картках
 *
 *карасель картинки мають змінюватися із за каруселі
 *
 *
 * спонсори
 *
 * - адекватна svg трикутника
 *
 */

const Header = () => {
  return (
    <>
      <section className={Container}>
        <BurgerMenu></BurgerMenu>
        <BlueZone></BlueZone>
        <YellowZone></YellowZone>
        <RedZone></RedZone>
        <BlackZone></BlackZone>
      </section>
    </>
  )
}

export default Header
