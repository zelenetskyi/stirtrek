//Core
import React, { useRef } from "react"

//Styles
import style from "./style.module.scss"

//Images
import black_widow from "../../images/black_widow.svg"
import vroom from "../../images/vroom.svg"
import what_do_ya_get from "../../images/what_do_ya_get.svg"
import play from "../../images/play.svg"
import next_yellow from "../../images/next_yellow.svg"
import Carousel from "../Carousel"

///Helpers
import { cumulativeOffset } from "../../helpers/calc"

const {
  Container,
  Wrapper,
  BlackWidow,
  Vroom,
  WhatWrapper,
  What,
  Watch,
  text,
  button,
  All,
  AllWrapper,
  AllContainer,
  nextIcon,
} = style

const RedZone = props => {
  const watchRef = useRef()

  const buttonMove = e => {
    if (watchRef) {
      const x =
        ((e.pageX - cumulativeOffset(watchRef).left - 450 / 2) * -1) / 100
      const y =
        ((e.pageY - cumulativeOffset(watchRef).top - 450 / 2) * -1) / 100

      const matrix = [
        [1, 0, 0, -x * 0.00005],
        [0, 1, 0, -y * 0.00005],
        [0, 0, 1, 1],
        [0, 0, 0, 1],
      ]

      watchRef.current.style.transform = `matrix3d(${matrix.toString()})`
    }
  }
  return (
    <section className={Container} onMouseMove={buttonMove}>
      <img
        src={black_widow}
        className={BlackWidow}
        alt="black_widow | STIR TREK"
      ></img>
      <img src={vroom} className={Vroom} alt="vroom | STIR TREK"></img>
      <div className={WhatWrapper}>
        <img
          src={what_do_ya_get}
          className={What}
          alt="what do ya get? | STIR TREK"
        ></img>
        <a href="/">
          <div>
            <div className={Watch} ref={watchRef}>
              <div className={button}>
                <img src={play}></img>
                <div className={text}>Watch Our Chanel</div>
              </div>
            </div>
          </div>
        </a>

        <div className={AllContainer}>
          <a href="/">
            <div className={AllWrapper}>
              <div className={All}>See All Events</div>
              <img
                src={next_yellow}
                className={nextIcon}
                alt="next icon | STIR TREK"
              ></img>
            </div>
          </a>
        </div>
      </div>
      <Carousel />
    </section>
  )
}

export default RedZone
