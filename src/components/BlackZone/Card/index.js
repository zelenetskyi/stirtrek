//Core
import React, { useRef } from "react"

//Styles
import style from "./style.module.scss"

//Images
import facebook_logo from "../../../images/facebook_logo.svg"
import insta_logo from "../../../images/insta_logo.svg"

//Helpers
import { cumulativeOffset } from "../../../helpers/calc"

const {
  Container,
  Card,
  Front,
  Back,
  SocialWrapper,
  Button,
  Content,
  Social,
  Text,
} = style

const CardComponent = ({ text, image = false }) => {
  const watchRef = useRef()

  const buttonMove = e => {
    if (watchRef) {
      const x =
        ((e.pageX - cumulativeOffset(watchRef).left - 50 / 2) * -1) / 100
      const y = ((e.pageY - cumulativeOffset(watchRef).top - 50 / 2) * -1) / 100

      console.log("sads")
      const matrix = [
        [1, 0, 0, -x * 0.000025],
        [0, 1, 0, -y * 0.000025],
        [0, 0, 1, 1],
        [0, 0, 0, 1],
      ]

      watchRef.current.style.transform = `matrix3d(${matrix.toString()})`
    }
  }
  return (
    <div className={Container}>
      <div className={Card}>
        <div className={Front}>
          <img
            src={
              image ||
              `https://u-recruit.com.ua/wp-content/uploads/2018/06/SENIOR-FRONT-END-WEB-DEVELOPER-1.jpg`
            }
            alt="third_card"
          ></img>
        </div>
        <div className={Back} onMouseMove={e => buttonMove(e)}>
          <div className={Content}>
            <a href="/">
              <div className={Button} ref={watchRef}>
                Enter now
              </div>
            </a>

            <div className={SocialWrapper}>
              <Link image={insta_logo} link="/"></Link>
              <Link image={facebook_logo} link="/"></Link>
            </div>
          </div>
        </div>
      </div>
      <div className={Text}>{text}</div>
    </div>
  )
}

export default CardComponent

const Link = ({ link, image }) => (
  <a href={link}>
    <img src={image} className={Social} alt="social | STIR TREK"></img>
  </a>
)

// <div>
// <div class="CardWrapper">
//   <div class="card">
//     <div class="front">
//       <img src={third_card} alt="text"></img>
//     </div>
//     <div class="back">
//       <span>Back</span>
//     </div>
//   </div>
// </div>
// </div>
