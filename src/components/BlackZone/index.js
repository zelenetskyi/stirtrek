//Core
import React, { useState, useEffect } from "react"

//Styles
import style from "./style.module.scss"

//Components
import Card from "../BlackZone/Card"
import AwesomeSlider from "react-awesome-slider"
// import AwsSliderStyles from "react-awesome-slider/src/styles"
import AwesomeSliderStyles from "react-awesome-slider/src/styled/open-animation"
// import "react-awesome-slider/dist/styles.css"
//Images
import boardgames from "../../images/boardgames.svg"
import mega_food from "../../images/mega_food.svg"
import "./slider.scss"

const {
  Container,
  BoarGames,
  CarWrapper,
  First,
  Third,
  Second,
  TextWrapper,
  annotation,
  motivation,
  title,
  MobileCarousel,
  Front,
  test,
  SlideText,
  SlideTextWrapper,
} = style

const TEXT = [
  `We're planning on a board 
game night for Thursday, 
April 25th.`,
  `We plan a hackathon for 
developers on December 27th`,
  `DEV 2020 Side quest for designers 
and developers`,
]

const BlackZone = props => {
  const [slide, setSlide] = useState(0)

  const changeSlide = i => setSlide(i)

  return (
    <section className={Container}>
      <img
        src={boardgames}
        className={BoarGames}
        alt="boardgames | STIR TREK"
      ></img>
      <div className={CarWrapper}>
        <div className={First}>
          <Card
            text={`We're planning on a board 
game night for Thursday, 
April 25th.`}
            image={`https://images.unsplash.com/photo-1493711662062-fa541adb3fc8?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80`}
          ></Card>
        </div>
        <div className={Second}>
          <Card
            text={`We plan a hackathon for 
developers on December 27th`}
            image={`https://images.unsplash.com/photo-1504384308090-c894fdcc538d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80`}
          ></Card>
        </div>
        <div className={Third}>
          <Card
            text={`DEV 2020 Side quest for designers 
and developers`}
            image={`https://images.unsplash.com/photo-1505373877841-8d25f7d46678?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1300&q=80`}
          ></Card>
        </div>
      </div>
      <div className={MobileCarousel}>
        <div className={Front}>
          <div className={test}></div>
          <AwesomeSlider
            cssModule={AwesomeSliderStyles}
            fillParent={true}
            onTransitionEnd={({ currentIndex }) => changeSlide(currentIndex)}
            play={true}
            interval={6000}
          >
            <div
              data-src={`https://images.unsplash.com/photo-1493711662062-fa541adb3fc8?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80`}
            ></div>
            <div
              data-src={`https://images.unsplash.com/photo-1504384308090-c894fdcc538d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80`}
            ></div>
            <div
              data-src={`https://images.unsplash.com/photo-1505373877841-8d25f7d46678?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1300&q=80`}
            ></div>
          </AwesomeSlider>
        </div>
        <div className={SlideTextWrapper}>
          <div className={SlideText}>{TEXT[slide]}</div>
        </div>
      </div>

      <div className={TextWrapper}>
        <img
          src={mega_food}
          className={title}
          alt="mega_food | STIR TREK"
        ></img>
        <h2 className={title}></h2>
        <h2 className={motivation}>
          Be awesome. Help those in need. Bring non-perishable food. Donate it.
          Maybe win something.
        </h2>
        <h3 className={annotation}>
          Any non-perishable donations welcomed, emphasis on shampoo,
          conditioner, canned tomatoes, canned beans and cereal. Donations will
          go to charity, and we'll be raffling off an awesome prize.
        </h3>
      </div>
    </section>
  )
}

export default BlackZone
