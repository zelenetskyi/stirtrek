//Core
import React from "react"

//Styles
import style from "./style.module.scss"

import Carousel from "./Carousel"

const { Container, Wrapper, Client } = style

const YellowZone = () => {
  return (
    <section className={Container}>
      <div className={Wrapper}>
        <Carousel></Carousel>
      </div>
      <h2 className={Client}>Customer Service: 727-228-1479</h2>
    </section>
  )
}

export default YellowZone
