//Core
import React, { useState, useEffect } from "react"

//Images
import awh from "../../../images/awh.svg"
// import awh from "../../../images/awh.svg"
import yellowBox from "../../../images/Yellow_Box.svg"
import boldpenguin from "../../../images/boldpenguin_350x200.png"

//Styles
import style from "./style.module.scss"

//Compoennts
import Slide from "./Slide"
import AwesomeSlider from "react-awesome-slider"
import AwesomeSliderStyles from "react-awesome-slider/src/styled/fall-animation/index"

// const Slider = () => (
//   <AwesomeSlider cssModule={AwesomeSliderStyles}>
//     {IMAGES.map((item, key) => (
//       <div key={key} data-src={item.image}>
//         <Slide {...item} image={""}></Slide>
//       </div>
//     ))}
//   </AwesomeSlider>
// )

const { ButtonContainer, QuickGoButtonsWrapper, QuickGoButton, Current } = style

const IMAGES = [
  {
    title: "sadas",
    sponsorImage: awh,
    sponsorName: "AWH",
    time: "111 days left",
    name: "STIR TREK - COLUMBUS",
    logolink: "/",
  },
  {
    title: "sadas",
    sponsorImage: boldpenguin,
    sponsorName: "boldpenguin",
    time: "111 days left",
    name: "STIR TREK - COLUMBUS",
    logolink: "/",
  },
  {
    title: "sadas",
    sponsorImage: awh,
    sponsorName: "AWH",
    time: "111 days left",
    name: "STIR TREK - COLUMBUS",
    logolink: "/",
  },
]

const TIME = 1000 * 15

const CarouselComponent = () => {
  const [currentImage, setCurrentImage] = useState(0)

  const changeActiveImage = index => setCurrentImage(index)

  useEffect(() => {
    const timer = setInterval(() => {
      setCurrentImage(currentImage + 1 >= IMAGES.length ? 0 : currentImage + 1)
    }, TIME)

    return () => {
      clearInterval(timer)
    }
  }, [currentImage])

  return (
    <div>
      <Slide
        {...IMAGES[currentImage]}
        currentNumber={currentImage}
        countOfSponsor={IMAGES.length}
        handleClick={changeActiveImage}
      />
      <div className={ButtonContainer}>
        <div className={QuickGoButtonsWrapper}>
          {Array(IMAGES.length)
            .fill(1)
            .map((_, index) => (
              <div
                key={index}
                onClick={() => changeActiveImage(index)}
                className={[
                  QuickGoButton,
                  index === currentImage ? Current : " ",
                ].join(" ")}
              ></div>
            ))}
        </div>
      </div>
    </div>
  )
}

export default CarouselComponent
