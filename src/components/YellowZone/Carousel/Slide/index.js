//Core
import React from "react"

//Styles
import style from "./style.module.scss"

import yellowBox from "../../../../images/Yellow_Box.svg"
import logoImage from "../../../../images/own_logo_sponsors.svg"
import next from "../../../../images/next.svg"
import awh from "../../../../images/awh.svg"

const {
  SlideWrapper,
  Image,
  InfoWrapper,
  Logo,
  Text,
  Info,
  LinkText,
  Link,
  TimeWrapper,
  SponsorsWrapper,
  SponsorLogo,
  Time,
  SponsorTitle,
  SponsorContent,
  SponsorName,
  NameWrapper,
  QuickGoButtonsWrapper,
  QuickGoButton,
  Current,
  nextIcon,
} = style

const Slide = ({
  title,
  sponsorImage = awh,
  countOfSponsor = 3,
  currentNumber = 1,
  handleClick = () => {},
  logolink = "/",
  sponsorName = "AWH",
  time = "111 days left",
  name = "STIR TREK - COLUMBUS",
  image,
}) => (
  <div className={SlideWrapper}>
    <img src={yellowBox} className={Image} alt={title}></img>
    <div className={SponsorsWrapper}>
      <div className={SponsorContent}>
        <div className={SponsorTitle}>General Sponsors</div>
        <div>
          <a href={logolink}>
            <img src={sponsorImage} className={SponsorLogo}></img>
          </a>
        </div>
        <a href={logolink}>
          <div className={NameWrapper}>
            <span className={SponsorName}>{sponsorName}</span>
            <img
              src={next}
              onClick={() => handleClick(currentNumber)}
              className={nextIcon}
              alt="next icon | STIR TREK"
            ></img>
          </div>
        </a>

        <div className={QuickGoButtonsWrapper}>
          {Array(countOfSponsor)
            .fill(1)
            .map((_, index) => (
              <div
                key={index}
                onClick={() => handleClick(index)}
                className={[
                  QuickGoButton,
                  index === currentNumber ? Current : " ",
                ].join(" ")}
              ></div>
            ))}
        </div>
      </div>
    </div>
    <div className={TimeWrapper}>
      <span className={Time}>{time}</span>
    </div>
    <a href="/" className={Link}>
      <div className={InfoWrapper}>
        <div className={Info}>
          <img src={logoImage} className={Logo} alt="logo | STIR TREK"></img>
          <div className={Text}>{name}</div>
        </div>
        <div>
          <a href="/" className={Link}>
            <div className={LinkText}>Enter Now </div>
            <img src={next} alt="next icon | STIR TREK"></img>
          </a>
        </div>
      </div>
    </a>
  </div>
)

export default Slide
